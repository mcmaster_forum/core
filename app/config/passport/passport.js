var bCrypt = require('bcrypt-nodejs');
var validator = require('validator');
var passwordValidator = require('password-validator');
var nodemailer = require('nodemailer');
var async = require('async');
var crypto = require('crypto');
var xoauth2 = require('xoauth2');

var schema = new passwordValidator();

schema
.is().min(8)
.is().max(20)
.has().uppercase()
.has().lowercase()
.has().digits()
.has().not().spaces()
.is().not().oneOf(['Passw0rd', 'Password123']);
 
module.exports = function(passport, user) {

    var User = user;
    var LocalStrategy = require('passport-local').Strategy;
    var FacebookStrategy = require('passport-facebook').Strategy;
    var configAuthFacebook = require('./facebookAuth');

    //LOCAL SIGNIN
    passport.use('local-signin', new LocalStrategy(
        {
            // by default, local strategy uses username and password, we will override with email
            usernameField: 'email',
            passwordField: 'password',
            passReqToCallback: true // allows us to pass back the entire request to the callback
        },


        function(req, email, password, done) {
            var User = user;
            var isValidEmailSyntax = function(email) {
                return validator.isEmail(email);
            };
            var isValidPasswordSyntax = function(password) {
                return schema.validate(password);
            };
            var isValidPassword = function(userpass, password) {
                return bCrypt.compareSync(password, userpass);
            };
            User.findOne({
                where: {
                    email: email
                }
            }).then(function(user) {
                if (!isValidEmailSyntax(email)) {
                    return done(null, false, {
                        message: 'Email is not valid.'
                    });
                }
                if (!isValidPasswordSyntax(password)) {
                    return done(null, false, {
                        message: 'Password is not valid.'
                    });
                }
                if (!user) {
                    return done(null, false, {
                        message: 'Email does not exist.'
                    });
                }
                if (!isValidPassword(user.password, password)) {
                    return done(null, false, {
                        message: 'Incorrect password.'
                    });
                }
                if (user.status == "inactive") {
                    return done(null, false, {
                        message: 'Email not verified. Please verify your email with the link in your Inbox.'
                    });
                }
                var userinfo = user.get();
                return done(null, userinfo);

            }).catch(function(err) {
                console.log("Error:", err);
                return done(null, false, {
                    message: 'Something went wrong with your SignIn' /* note2Self: check this code */
                });
            });
        }
    ));
 
    // LOCAL SIGNUP
    passport.use('local-signup', new LocalStrategy({
        usernameField: 'email',
        firstnameField: 'firstname',
        lastnameField: 'lastname',
        passwordField: 'password',
        passReqToCallback: true // allows us to pass back the entire request to the callback
    }, function(req, email, password, done) {
        var isValidEmail = function(email) {
            return validator.isEmail(email);                
        };
        var isValidFirstname = function() {
            return validator.isAlpha(req.body.firstname);
        };
        var isValidLastname = function() {
            return validator.isAlpha(req.body.lastname);
        };
        var isValidPasswordSyntax = function(password) {
            return schema.validate(password);
        };
        var generateHash = function(password) {
            return bCrypt.hashSync(password, bCrypt.genSaltSync(8), null);
        };

        var token = crypto.randomBytes(20).toString('hex');

        User.findOne({
            where: {
                email: email
            }
        }).then(function(user) {
            if (!isValidEmail(email)) {
                return done(null, false, {
                    message: 'Email format is not valid'
                });
            }
            if (!isValidFirstname()) {
                return done(null, false, {
                    message: 'First Name is not valid'
                });
            }
            if (!isValidLastname()) {
                return done(null, false, {
                    message: 'Last Name is not valid'
                });
            }
            if (!isValidPasswordSyntax(password)) {
                return done(null, false, {
                    message: 'That password is not valid.'
                });
            }
            if (user) {
                console.log('ERROR');
                return done(null, false, {
                    message: 'Sorry! That email is already taken.'
                });

            } else {
                console.log('CONTINUE');
                var userPassword = generateHash(password);

                var data = {
                    email: email,
                    emailVerification: token,
                    emailVerificationExpires: Date.now() + 3600000, /* expires in 1 day */
                    password: userPassword,
                    firstname: req.body.firstname,
                    lastname: req.body.lastname
                };

                User.create(data).then(function(newUser) {
                    if (!newUser) {
                        return done(null, false);
                    }   
                    if (newUser) {
                        var transporter = nodemailer.createTransport({
                            service: 'gmail',
							host: 'smtp.gmail.com',
							port: 465,
							secure: true,  /* true for 465, false for other ports */
                            auth:{
                                user: 'forum.mcmaster@gmail.com',
                                pass: 'A10ShunOGr81'
                            }
                        });
                        var mailOptions = {
                            to: newUser.email,
                            from: '"Health Systems Exchange" <ryanrhunter@gmail.com>',
                            subject: 'HSE Account Registration',
                            text: 'You are receiving this because you (or someone else) have requested the creation of an account on portal. A registration email has been mailed to '+ newUser.email +'. Thanks. \n\n' +
                            'Please click on the following link, or paste this into your browser to complete the process:\n\n' +
                            'http://' + req.headers.host + '/email-verification/' + token + '\n\n' +
                            'If you did not request this, please ignore this email.\n'
                        };
                        transporter.sendMail(mailOptions, function () {
                            done(null, false, {
                                message: 'An email has been sent to ' + newUser.email + ' with further instructions.'
                            });
                            req.session.save();
                            return transporter.close();
                        });
                    }

                }).catch(function(error) {
                    console.log("Error:", error);
                });
            }
        }).catch(function(error) {
            console.log("Error:", error);
        });
    }));

    /* Authentication via Social Media */
	/* ------------------------------- */
	// FACEBOOK LOGIN/SIGNUP
	/* note2Self:
	Get local authentication working first, then circle back to the social media authentication later.
    passport.use('facebook', new FacebookStrategy({

        // pull in our app id and secret from our auth.js file
        clientID        : configAuthFacebook.facebookAuth.clientID,
        clientSecret    : configAuthFacebook.facebookAuth.clientSecret,
        callbackURL     : configAuthFacebook.facebookAuth.callbackURL,
        profileFields   : configAuthFacebook.facebookAuth.profileFields,
        scope           : configAuthFacebook.facebookAuth.scope,
        enableProof     : true

    }, function(token, refreshToken, profile, done) {

        // asynchronous
        process.nextTick(function() {

            // find the user in the database based on their facebook id
            User.findOne({
                where: {
                    fb: profile.id
                }
            }).then(function(err, user) {

                // if there is an error, stop everything and return that
                // ie an error connecting to the database
                if (err) {
                    done(err);
                }

                console.log(profile.id);

                if(!user) {
                    // if there is no user found with that facebook id, create them
                    console.log('Create User')
                    var newUser            = new User();

                    // set all of the facebook information in our user model
                    newUser.fb    = profile.id; // set the users facebook id                   
                    newUser.token = token; // we will save the token that facebook provides to the user                    
                    newUser.firstname  = profile.name.givenName; // look at the passport user profile to see how names are returned
                    newUser.lastname  = profile.name.familyName; // look at the passport user profile to see how names are returned
                    newUser.email = profile.emails[0].value; // facebook can return multiple emails so we'll take the first

                    // save our user to the database
                    newUser.save(function(err) {
                        if (err) throw err;

                        // if successful, return the new user
                        //console.log('Create User and Continue')
                        done(null, newUser);
                    });

                } else {
                    console.log('Forward')
                    done(null, user);
                }
            });
        });
    }));
	***********/




    /* -------------------------------- */
	//serialize
    passport.serializeUser(function(user, done) {     
        done(null, user.id);     
    });

    // deserialize user 
    passport.deserializeUser(function(id, done) {     
        User.findById(id).then(function(user) {     
            if (user) {     
                done(null, user.get());     
            } else {     
                done(user.errors, null);     
            }     
        });     
    });
};
