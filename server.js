// start and main page

// require our dependencies
const express 	= require('express');
var   app 		= express();
const path		= require('path');
const PORT 		= process.env.PORT || 5003;
const port 		= process.env.port || 5003; 
const webPort	= PORT;
const siteName	= "Health Systems Exchange";

var env = require('dotenv').load();  /* loads environment variables from .env file. Safer to store configuration configuration data in .ev file. Ensure to exclude file in .gitnore file */
var session = require('express-session'); // session middleware
var cookieParser = require('cookie-parser'); // parse cookies
var passport = require('passport'); /* local & social media authentication library */
var bodyParser = require('body-parser'); // parse posts
var exphbs = require('express-handlebars'); /* handlebars templating.  I'll probably switch to EJS later */
var flash = require('express-flash'); // notification messages
var morgan = require('morgan'); // logger

 
/* Configure our base Express application */
app.use(bodyParser.urlencoded({extended: true})); /* get information from html forms */
app.use(bodyParser.json());
app.use(cookieParser());

// For Passport
app.use(session({
    secret: 'v23523pathais35alwaysr723i3472d88c9n0247f99dn9dn29dn70xb90302bd23unferxigt',
    resave: true,
    saveUninitialized: true
})); // session secret
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions

app.use(flash()); /* use express-flash for flash messages stored in session. */
app.use(morgan('dev')); /* log every request to the Dev console */

 
//For Handlebars
app.set('views', './app/views');
app.engine('hbs', exphbs({
    extname: '.hbs'
}));

app.set('view engine', '.hbs');
app.use(express.static(__dirname + '/public'));

// Data Models
var models = require("./app/models");
 
// Routing
var routes = require('./app/routes/routes.js')(app, passport);
var authRoute = require('./app/routes/auth.js')(app, passport, models.user); 
 
// load passport strategies
require('./app/config/passport/passport.js')(passport, models.user);
 
 
// Sync Database
models.sequelize.sync().then(function() {
    console.log('Excellent! Our database and DB connection looks fine. Onwards and upwards!!')
}).catch(function(err) {
    console.log(err, "Oh, no!! Something went wrong with the Database Update!")
});
 
 
 
// Start up our web application  
app.listen(port, function(err) {
    if (!err)
        console.log(`HSE - ${siteName} now started and available on port ${webPort} at http://localhost:${webPort}/ Enjoy!`);
    else console.log(err)
});
