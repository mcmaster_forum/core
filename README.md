# HSE Web App
hsessewebapp ~ 
A web login and authentication app.  Back-end in Node.js with Express framework and PostgreSQL database and Sequelize ORM.  Front-end in React.js.  Authentication by the Passport.js library. Also uses Bootstrap 4, Google Fonts, and a Fontastic.me generated font.  Property of McMaster - HSE (Health Systems Evidence).

## Steps to Install and Run Locally
Make sure you have [Node.js](http://nodejs.org/) and the [Heroku CLI](https://cli.heroku.com/) installed.

- git clone https://gitlab.com/mcmaster_forum/hsewebapp.git    
- npm install
- Verify (and adjust if needed) the database configuration, and nodemailer configuration.
- npm start.  Or if you like, npm server.js OR nodemon server.js OR node server.js 
- Runs on port 5000 --> http://localhost:5000/ 
- Your app should now be running on [localhost:5000](http://localhost:5000/). 
- Links to test include http://localhost:5000/ , http://localhost:5000/signup , http://localhost:5000/dashboard , http://localhost:5000/signin , http://localhost:5000/logout , and the assorted content pages that should be available only from the home Dashboard upon successful login.
- Still to come!  The "Password Reset Email" feature is currently a Work in Progress, and will be uploaded in 1 - 2 days. Further cosmetic styling of the login and signup forms are in progress.
- There is a known error issue with sequelize.  Please use these specific versions of sequelize and sequelize-cli. Run 'npm install --save sequelize@4.38.0' and 'npm install --save sequelize-cli@3.1.0'.  See comment by 'alexpmorris' dated 2017/11/02 at https://github.com/sequelize/sequelize/issues/7977 .



## Features
- Node.js
- Bootstrap 4
- Fontastic.me custom generated vector icons
- Google Fonts


## License
This project is licensed under the terms of the **MIT** license.
![HSE Start Page](http://ryanhunter.org/images/portfolio/hse/00_hse_homepage.png)



## Other Developer Notes
- Uses the Passport.js authentication library, and the Sequelize ORM.  See http://www.passportjs.org/ and http://docs.sequelizejs.com/ 
- There is a known error issue with sequelize.  Please use these specific versions of sequelize and sequelize-cli. Run 'npm install --save sequelize@4.38.0' and 'npm install --save sequelize-cli@3.1.0'.  See comment by 'alexpmorris' dated 2017/11/02 at https://github.com/sequelize/sequelize/issues/7977 . 
- Made DB connection notes in config/passport/passport.js file, since I can't do comments in the associated config.JSON file.  
- Use the supplied supporting SQL text file to create the initial PostgreSQL database and DB user.  See "config" sub-folder or https://gitlab.com/mcmaster_forum/hsewebapp/blob/master/config/supporting_SQL_Code.sql.TXT   
- Sequelize ORM will create the tables and fields. 
- Database name on localhost only is "hsessedb" and DB user is "hsesseuser".  On Heroku, the database credentials are stored in the specific deployed app settings under the data configuration settings.  After adding PostgreSQL as an "add on" option of course. For more details, see the config/passport/passport.js file.    
- Consider using .env file at web root to store the DB credentials in.  If so, remember to add .env to the .gitnore file, so credentials don't get accidentally posted to GitLab!!!  
- Database password emailed to manager for safe-keeping.  


## Deploying to Heroku
Make sure you have [Node.js](http://nodejs.org/) and the [Heroku CLI](https://cli.heroku.com/) installed.  Ensure there is a Procfile in the root directory containing the following text "web: node server.js" only.

```
$ heroku create
$ git push heroku master
$ heroku open
```
or

[![Deploy to Heroku](https://www.herokucdn.com/deploy/button.png)](https://heroku.com/deploy)


## Screen-shots

![HSE Start Page](http://ryanhunter.org/images/portfolio/hse/00_hse_homepage.png)

![HSE Start Page](http://ryanhunter.org/images/portfolio/hse/01_signup.png)

![HSE Start Page](http://ryanhunter.org/images/portfolio/hse/02_dashboard_01.png)

![HSE Start Page](http://ryanhunter.org/images/portfolio/hse/03_dashboard_02.png)

![HSE Start Page](http://ryanhunter.org/images/portfolio/hse/04_signin.png)

![HSE Start Page](http://ryanhunter.org/images/portfolio/hse/05_sql_results.png)

![HSE Start Page](http://ryanhunter.org/images/portfolio/hse/06_form_validation_frontend_01.png)

![HSE Start Page](http://ryanhunter.org/images/portfolio/hse/07_form_validation_frontend_02.png)

![HSE Start Page](http://ryanhunter.org/images/portfolio/hse/08_db_credentials.png)

